package cpe.projet.erudilyon.wsstore;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ContextConfiguration;

@ContextConfiguration(classes = ErudiLyonWsStoreApplication.class)
@DataJpaTest
class ErudiLyonWsStoreApplicationTests {

    @Test
    void contextLoads() {
    }

}
