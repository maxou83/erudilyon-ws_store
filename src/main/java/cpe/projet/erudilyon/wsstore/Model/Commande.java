package cpe.projet.erudilyon.wsstore.Model;

import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table(name = "T_E_COMMANDE_COMMANDE")
@Data
public class Commande {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "COMMANDE_ID")
    private Integer idCommande;

    @Column(name = "DATE_COMMANDE")
    private Date dateCommande;

    @Column(name = "MONTANT_COMMANDE")
    private BigDecimal montantCommande;

    @Column(name = "ARTICLE_COMMANDE")
    private Integer idArticle;

    @Column(name = "USER_COMMANDE")
    private Integer idUser;

}
