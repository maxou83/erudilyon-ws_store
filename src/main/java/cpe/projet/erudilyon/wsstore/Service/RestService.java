package cpe.projet.erudilyon.wsstore.Service;

import Model.ActivityUserTransactionDTO;
import Model.ArticleDTO;
import Model.UserDTO;
import Validation.DataValidation;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

@Service
public class RestService {

    private final RestTemplate restTemplate;
    //private final String baseUrl = "http://localhost";
    private final String baseUrl = "http://asi-projet-cpe.northeurope.cloudapp.azure.com";

    public RestService(RestTemplateBuilder restTemplateBuilder) {
        this.restTemplate = restTemplateBuilder.build();
    }

    public boolean checkQteArticle(Integer idArticle) {

        ArticleDTO articleDTO = getArticle(idArticle);

        if(articleDTO.getQteArticle() > 0){
            return true;
        }
        return false;

    }

    public BigDecimal getMontantArticle(Integer idArticle) {
        ArticleDTO articleDTO = getArticle(idArticle);
        return articleDTO.getPrixArticle();
    }

    public DataValidation processQteArticle(Integer idArticle) {

        String url = baseUrl + ":8085/edit";

        ArticleDTO articleDTO = getArticle(idArticle);
        Integer qteFinalArticle = articleDTO.getQteArticle() - 1;

        articleDTO.setQteArticle(qteFinalArticle);
        if(qteFinalArticle==0) {
            articleDTO.setEtatArticle(0);
        }

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        headers.setBasicAuth("admin", "admin");



        HttpEntity<ArticleDTO> entity = new HttpEntity<>(articleDTO, headers);
        HttpEntity<DataValidation> response = restTemplate.exchange(url, HttpMethod.PUT, entity, DataValidation.class);

        return response.getBody();
    }

    public DataValidation updateSoldeUser(Integer idUser, BigDecimal montantArticle) {

        String url = baseUrl + ":8084/substractSolde";

        ActivityUserTransactionDTO activityUserTransactionDTO = new ActivityUserTransactionDTO();
        activityUserTransactionDTO.setUserId(idUser);
        activityUserTransactionDTO.setAmount(montantArticle);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        headers.setBasicAuth("admin", "admin");


        HttpEntity<ActivityUserTransactionDTO> entity = new HttpEntity<>(activityUserTransactionDTO, headers);
        HttpEntity<DataValidation> response = restTemplate.exchange(url, HttpMethod.PUT, entity, DataValidation.class);

        return response.getBody();
    }

    private ArticleDTO getArticle(Integer idArticle){
        String url = baseUrl + ":8085/get/{id}";

        Map<String, String> params = new HashMap<>();
        params.put("id", idArticle.toString());

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        headers.setBasicAuth("admin", "admin");

        HttpEntity<ActivityUserTransactionDTO> entity = new HttpEntity<>(headers);
        HttpEntity<ArticleDTO> response = restTemplate.exchange(url, HttpMethod.GET, entity, ArticleDTO.class, params);

        return response.getBody();
    }

    public UserDTO getUser(Integer idUser){
        String url = baseUrl + ":8084/get/{id}";

        Map<String, String> params = new HashMap<>();
        params.put("id", idUser.toString());


        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        headers.setBasicAuth("admin", "admin");

        HttpEntity<ActivityUserTransactionDTO> entity = new HttpEntity<>(headers);
        HttpEntity<UserDTO> response = restTemplate.exchange(url, HttpMethod.GET, entity, UserDTO.class, params);

        return response.getBody();
    }
}
