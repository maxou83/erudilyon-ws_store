package cpe.projet.erudilyon.wsstore.Service;

import Model.ArticleDTO;
import Model.CommandeDTO;
import Validation.DataValidation;
import cpe.projet.erudilyon.wsstore.Model.Commande;
import cpe.projet.erudilyon.wsstore.Repository.CommandeRepository;
import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.Date;

@Service
public class CommandeService {

    @Autowired
    CommandeRepository commandeRepository;

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private RestService restService;

    public DataValidation buyArticle(CommandeDTO commandeDTO) {
        DataValidation dataValidation = new DataValidation();
        dataValidation.setElem(Commande.class.getName());

        //On vérifie si la quantité d'articles est suffisante pour l'achat
        System.out.println("[LOG] - Vérification de la quantité d'article restante");
        if (!restService.checkQteArticle(commandeDTO.getIdArticle())) {
            dataValidation.setStatus("EMPTY_STOCK");
            dataValidation.setValid(false);
            return dataValidation;
        }
        // Récupération du montant de l'article
        BigDecimal montantArticle = restService.getMontantArticle(commandeDTO.getIdArticle());

        //On vérifie que l'utilisateur a le bon nombre de points
        if(restService.getUser(commandeDTO.getIdUser()).getSolde().compareTo(montantArticle) < 0){
            dataValidation.setStatus("INSUFFICIENT_BALANCE");
            dataValidation.setValid(false);
            return dataValidation;
        }

        // On décrémente le nombre d'article
        restService.processQteArticle(commandeDTO.getIdArticle());



        //On passe la commande
        try {
            commandeRepository.save(convertToEntity(commandeDTO, montantArticle));
        } catch (ParseException p) {
            dataValidation.setStatus("INTERNAL_ERROR");
            dataValidation.setValid(false);
            return dataValidation;
        }

        // On met à jour les informations de l'utilisateur
        restService.updateSoldeUser(commandeDTO.getIdUser(), montantArticle);

        // On communique l'état
        dataValidation.setElem(Commande.class.getName());
        dataValidation.setStatus("TRANSACTION_OK");
        dataValidation.setValid(true);

        return dataValidation;
    }

    private CommandeDTO convertToDto(Commande commande) {
        CommandeDTO commandeDTO = modelMapper.map(commande, CommandeDTO.class);
        return commandeDTO;
    }

    private Commande convertToEntity(CommandeDTO commandeDTO, BigDecimal montant) throws ParseException {
        modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);

        Commande commande = modelMapper.map(commandeDTO, Commande.class);
        commande.setDateCommande(new Date());
        commande.setMontantCommande(montant);
        return commande;
    }

}
