package cpe.projet.erudilyon.wsstore.Controller;


import Model.CommandeDTO;
import Validation.DataValidation;
import cpe.projet.erudilyon.wsstore.Model.Commande;
import cpe.projet.erudilyon.wsstore.Service.CommandeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
@Api(value = "API de Gestion des Commandes")
public class CommandeController {

    @Autowired
    CommandeService commandeService;

    @ApiOperation(value = "Permet de controler la disponibilité du service")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Le service est en ligne"),
            @ApiResponse(code = 401, message = "Vous n'êtes pas autorisé à accéder à cette ressource"),
            @ApiResponse(code = 403, message = "L'accès à cette ressource est interdit"),
            @ApiResponse(code = 404, message = "La ressource demandé n'a pas été trouvé")
    })
    @GetMapping("/up")
    public String up() {
        return "UP";
    }


    @ApiOperation(value = "Effectue une transaction (User achète Article)", response = DataValidation.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Le service est en ligne"),
            @ApiResponse(code = 401, message = "Vous n'êtes pas autorisé à accéder à cette ressource"),
            @ApiResponse(code = 403, message = "L'accès à cette ressource est interdit"),
            @ApiResponse(code = 404, message = "La ressource demandé n'a pas été trouvé")
    })
    @PostMapping("/buyArticle")
    @PostAuthorize("hasAnyRole('USER','ADMIN','PARTNER')")
    public DataValidation buyArticle(@RequestBody CommandeDTO commandeDTO) {

        return commandeService.buyArticle(commandeDTO);
    }

}
