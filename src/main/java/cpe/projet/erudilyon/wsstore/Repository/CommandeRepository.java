package cpe.projet.erudilyon.wsstore.Repository;

import cpe.projet.erudilyon.wsstore.Model.Commande;
import org.springframework.data.repository.CrudRepository;

public interface CommandeRepository extends CrudRepository<Commande, Integer> {
}
