package cpe.projet.erudilyon.wsstore;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableSwagger2
public class ErudiLyonWsStoreApplication {

    public static void main(String[] args) {
        SpringApplication.run(ErudiLyonWsStoreApplication.class, args);
    }

}
